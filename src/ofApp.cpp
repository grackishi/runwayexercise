#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	// Set window to be same shape as image from runway!
	ofSetWindowShape(1024, 1024);

	// Setup runway so that it actually works
	runway.setup(this, "http://localhost:8001");
	runway.start();

	// Fill vector with random values to start
	for (int i = 0; i < 512; i++) {
		float f = ofRandom(-0.5, -0.5);
		vect.push_back(f);
	}
}

//--------------------------------------------------------------
void ofApp::update(){
	// Make sure runway is not busy, then give it more stuff
	if (!runway.isBusy()) {
		ofxRunwayData data; 
		int frame = ofGetFrameNum();

		// Change all the floats in the vector by 0.01 to see change in image
		for (int i = 0; i < 512; i++) {
			// Change the floats by a random numberr between -.1 and .1 instead of using noise
			// Using draw a dino and it looks like a little animation
			vect[i] += ofRandom(-.1, 0.1);
		}

		// Load vect into data
		data.setFloats("z", vect);

		// Send Data to runway!
		runway.send(data);
	}

	runway.get("image", runResult);

}

//--------------------------------------------------------------
void ofApp::draw(){
	// Draw runway result!
	if (runResult.isAllocated()) {
		runResult.draw(0, 0);
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	// Liked this functionality so stole from example code
	if (key == ' ') { //press spacebar to generate a new vector
		vect.clear(); //clear the vector
		for (int i = 0; i < 512; i++) {
			float n = ofRandom(-3.0, 3.0); //load the vector with new floats
			vect.push_back(n);
		}

	}
}

void ofApp::runwayInfoEvent(ofJson& info) {
	ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message) {
	ofLogNotice("ofApp::runwayErrorEvent") << message;
}
