#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp, public ofxRunwayListener{

	public:
		void setup();
		void update();
		void draw();

		void keyReleased(int key);

		ofxRunway runway;

		ofImage runResult;
		vector<float> vect;

		void runwayInfoEvent(ofJson& info);
		void runwayErrorEvent(string& message);
		
};
