# Runway Exercise #

This exercise takes data from runway in the form of a vector and changes each float by a random float between .1 and -.1.
I used the hand drawn dinosaur GAN so every change is small enough to where it looks like a little animated dinosaur which 
is fun. I also used the spacebar functionality from the example to generate a new starting point for the dinosaur picture if
the user gets bored with it. This exercise made me examine the example programs and understand how using runway in OpenFrameworks
works and gave me some ideas how I can use it in the future. 